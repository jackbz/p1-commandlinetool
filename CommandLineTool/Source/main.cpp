//
//  main.cpp
//  CommandLineTool
//
//  Created by Tom Mitchell on 08/09/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#include <iostream>

void List(int Num);

int main()
{
    int intNum;
    
    while(true)
    {
        std::cout << "Type in a positive number and hit enter\n";
        std::cin >> intNum;
        
        List(intNum);
    }
    
    
    return 0;
}

void List(int Num)
{
    int factoral=1;
    
    if (Num > -1)
    {
        for (int index = 1; index < Num+1; index++)
            {
                factoral *= index;
            }
            std::cout << "Factorial of " << Num << " = " << factoral << "\n";
    }
        else
            std::cout << "Hey, that is a negative number!\n";
}
